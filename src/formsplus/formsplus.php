<?php
/**
 * Plugin to add Forms+ CSS & Javascript to wordpress
 *
 * @link              https://codecanyon.net/item/forms-plus-js-forms-framework
 *
 * @wordpress-plugin
 * Plugin Name:       Forms+
 * Description:      Plugin to add Forms+ CSS & Javascript to wordpress
 * Version:           1.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}


function activate_formsplus() {
}
register_activation_hook( __FILE__, 'activate_formsplus' );

function deactivate_plugin_name() {
}
register_deactivation_hook( __FILE__, 'deactivate_formsplus' );

/**
 * Enqueue scripts and styles
 */
function formsplus_scripts() {
		wp_enqueue_style( 'formsplus-bootstrap', plugin_dir_url( __FILE__ ) . '/bootstrap/css/bootstrap.min.css' );
		wp_enqueue_style( 'formsplus', plugin_dir_url( __FILE__ ) . '/css/formsplus.min.css' );
		
		wp_enqueue_script( 'formsplus', plugin_dir_url( __FILE__ ) . '/js/formsplus.min.js', array('jquery', 'jquery-ui-core', 'jquery-ui-mouse'), null, true );
}
add_action( 'wp_enqueue_scripts', 'formsplus_scripts' );
