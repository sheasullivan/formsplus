var gulp = require('gulp'); 
//var plumber = require('gulp-plumber');
var del = require('del');
var cssnano = require('gulp-cssnano');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var zip = require('gulp-zip');

gulp.task('default', ['zip'], function() {
});

gulp.task('zip', ['cssnano', 'uglify', 'php', 'bootstrap'], function() {
	return gulp.src('./build/**/*')
		.pipe(zip('formsplus.zip'))
		.pipe(gulp.dest('./build'));
});

gulp.task('bootstrap', function() {
	return gulp.src('./src/formsplus/bootstrap/**/*')
		.pipe(gulp.dest('./build/formsplus/bootstrap'));
});

gulp.task('cleanphp', function() {
	return del('./build/formsplus/*.php');
});

gulp.task('php', ['cleanphp'], function() {
	return gulp.src('./src/formsplus/*.php')
		.pipe(gulp.dest('./build/formsplus'));
});

gulp.task('cssnano', ['cleancss'], function(){
  return gulp.src([
		'./js/spectrum/spectrum.css',
		'./js/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
		'./js/jquery-ui/css/core.css',
		'./js/jquery-ui/css/slider.css',
		'./js/jquery-ui/css/menu.css',
		'./js/jquery-ui/css/autocomplete.css',
		'./css/forms-plus.css',
		'./css/color/forms-plus-extra2color3.css'],
		{ cwd: './src/formsplus' })
    //.pipe(plumber())
    .pipe(concat('formsplus.min.css'))
    .pipe(cssnano({discardComments: {removeAll: true}}))
    .pipe(gulp.dest('./build/formsplus/css/'));
}); 

gulp.task('cleancss', function() {
	return del('./build/formsplus/css/*.css');
});

gulp.task('uglify', ['cleanjs'], function(){
  return gulp.src([
		'./js/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js',
		'./js/moment/moment.js',
		'./js/moment/locale/da.js',
		'./js/jquery-validation/jquery.validate.js',
		'./js/jquery-validation/additional-methods.js',
		'./js/jquery-masked-input/jquery.maskedinput.js',
		'./js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js',
		'./js/jquery-realperson/jquery.plugin.js',
		'./js/jquery-realperson/jquery.realperson.js',
		'./js/spectrum/spectrum.js',
		'./js/jquery-form-plugin/jquery.form.js',
		'./js/forms-plus/forms-plus.js',
		'./js/forms-plus/forms-plus-value.js',
		'./js/forms-plus/forms-plus-field.js',
		'./js/forms-plus/forms-plus-file.js',
		'./js/forms-plus/forms-plus-spinner.js',
		'./js/forms-plus/forms-plus-validation.js',
		'./js/forms-plus/forms-plus-block.js',
		'./js/forms-plus/forms-plus-elements.js',
		'./js/forms-plus/forms-plus-gelements.js',
		'./js/forms-plus/forms-plus-slider.js',
		'./js/forms-plus/forms-plus-autocomplete.js',
		'./js/forms-plus/forms-plus-ajax.js',
		'./js/forms-plus/forms-plus-steps.js',
		'./js/script.js'],
		{ cwd: './src/formsplus' })
    //.pipe(plumber())
    .pipe(concat('formsplus.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./build/formsplus/js/'));
}); 

gulp.task('cleanjs', function() {
	return del('./build/formsplus/js/*.js');
}); 
